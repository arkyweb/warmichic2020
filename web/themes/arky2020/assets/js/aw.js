WebFont.load({
    google: {
        families: ['Signika:300,400,700']
    }
});


//
const nav = document.querySelector('.home');
//const navTop = nav.offsetTop;
function stickyNavigation() { 

  if (window.scrollY >= 10) { /* navTop */
    document.body.classList.add('arkywarmi');
  } else { 
    document.body.classList.remove('arkywarmi');
  }
};
window.addEventListener('scroll', stickyNavigation);


// letras home

if (document.body.classList.contains('home')) {

var textPath = document.querySelector('#text-path');
var textContainer = document.querySelector('#text-container');
var path = document.querySelector( textPath.getAttribute('href') );
var pathLength = path.getTotalLength(); 
function updateTextPathOffset(offset){
  textPath.setAttribute('startOffset', offset); 
}
updateTextPathOffset(pathLength);

function onScroll(){
  requestAnimationFrame(function(){
    var rect = textContainer.getBoundingClientRect();
    var scrollPercent = rect.y / window.innerHeight;
    console.log(scrollPercent);
    updateTextPathOffset( scrollPercent * 2 * pathLength );
  });
}

window.addEventListener('scroll',onScroll);
  
}

// change menu color

var color = new Array();
color[0] = "#000035";
color[1] = "#C65839";
color[2] = "#B3378A";
color[3] = "#E4AE00";
color[4] = "#74AFC9";
color[5] = "#BC8815";
color[6] = "#5873B0";
color[7] = "#C27197"; 

function changeColor() {
    var lastColorIndex = localStorage.getItem('lastColorIndex') || -1;
    var randomColor = -1;
    while(lastColorIndex == randomColor || randomColor === -1) {        
      randomColor = Math.floor(Math.random() * color.length); 
    };
    localStorage.setItem('lastColorIndex',randomColor); 
    document.getElementsByClassName("canvas-bar")[0].style.backgroundColor=color[randomColor];
};
changeColor();  // do this in window.onload !!  window.onload(changeColor)